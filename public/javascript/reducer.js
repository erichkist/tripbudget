import * as trip from './trip/actions.js'
import * as transaction from './transaction/actions.js'
import * as stats from './stats/actions.js'

import store from './store.js'

const today = new Date()

const defaultState = {
  budget: 0,
  days: 0,
  dailyBudget: 0,
  startAt: today,
  endAt: today,
  transactions: {}
}

export const dispatch = (event, attributes = {}) => {
  const state = store.read() || defaultState
  const newState = reducer({ type: event, attributes: attributes }, state)

  if (state == newState) return

  stats.update(newState)
  store.write(newState)
}

export const reducer = ({ type, attributes }, state) => {
  switch (type) {
    case 'app.start':
      trip.load(state)
      transaction.load(state)
      stats.load(state)

      return state

    case 'trip.update.budget':
      return trip.updateBudget(state, attributes)

    case 'trip.update.dailyBudget':
      return trip.updateDailyBudget(state, attributes)

    case 'trip.update.period':
      return trip.updatePeriod(state, attributes)

    case 'trip.delete':
      transaction.clear()
      trip.load(defaultState)

      return defaultState

    case 'transaction.new':
      transaction.startNew()
      return state

    case 'transaction.save':
      return transaction.save(state, attributes)

    case 'transaction.edit':
      transaction.edit(state, attributes)
      return state

    case 'transaction.remove':
      return transaction.remove(state, attributes)

    default:
      console.log('unknonw event', type)
      return state
  }
}
