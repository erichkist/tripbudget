const store = {}

const dateKeys = [
  'startAt',
  'endAt',
  'timestamp'
]

store.read = () => {
  const raw = localStorage['trip']

  if (!raw) {
    return store.default
  }

  return JSON.parse(raw, (key, value) => {
    if (dateKeys.includes(key)) {
      return new Date(value)
    }

    return value
  })
}

store.write = (data) => {
  const newTrip = { ...store.read(), ...data }

  localStorage['trip'] = JSON.stringify(newTrip)
}

store.remove = () => {
  delete localStorage['trip']
}

export { store as default }
