import { filterFloat } from '../lib/math.js'
import { ui } from './ui.js'
import { dispatch } from '../reducer.js'

export const startEventListners = () => {
  ui.form.addEventListener('reset', dispatchTripRemove)

  ui.budget.addEventListener('keyup', dispatchTripUpdateBudget)
  ui.dailyBudget.addEventListener('keyup', dispatchTripUpdateDailyBudget)

  ui.startAt.addEventListener('change', dispatchTripUpdatePeriod)
  ui.endAt.addEventListener('change', dispatchTripUpdatePeriod)

  ui.includeStartAt.addEventListener('change', dispatchTripUpdatePeriod)
  ui.includeEndAt.addEventListener('change', dispatchTripUpdatePeriod)
}

const dispatchTripRemove = (event) => {
  event.stopPropagation()
  event.preventDefault()

  if (confirm("This cannot be undone! is Are you sure?")) {
    dispatch('trip.delete')
  }
}

const dispatchTripUpdateBudget = () =>
  dispatch('trip.update.budget', {
    budget: filterFloat(ui.budget.value)
  })


const dispatchTripUpdateDailyBudget = () =>
  dispatch('trip.update.dailyBudget', {
    dailyBudget: filterFloat(ui.dailyBudget.value)
  })


const dispatchTripUpdatePeriod = () =>
  dispatch('trip.update.period', {
    startAt: ui.startAt.valueAsDate,
    includeStartAt: ui.includeStartAt.checked,
    endAt: ui.endAt.valueAsDate,
    includeEndAt: ui.includeEndAt.checked,
  })
