import { round, daysBetween } from '../lib/math.js'
import { dateFormatter } from '../lib/formatters.js'
import { ui } from './ui.js'

export const load = (state) =>
  updateUI(state)

export const updateBudget = (state, attributes) => {
  if (isNaN(attributes.budget) || attributes.budget <= 0) {
    return { ...state, budget: attributes.budget }
  }

  const trip = { ...state, budget: attributes.budget }

  trip.dailyBudget = round((trip.budget || 0) / (trip.days || 1))

  return updateUI(trip)
}

export const updateDailyBudget = (state, attributes) => {
  if (isNaN(attributes.dailyBudget) || attributes.dailyBudget <= 0) {
    return { ...state, dailyBudget: attributes.dailyBudget }
  }

  const trip = { ...state, dailyBudget: attributes.dailyBudget }

  trip.budget = round((trip.dailyBudget || 0) * (trip.days || 1))

  return updateUI(trip)
}

export const updatePeriod = (state, attributes) => {
  const trip = {
    ...state,
    startAt: attributes.startAt,
    includeStartAt: attributes.includeStartAt,
    endAt: attributes.endAt,
    includeEndAt: attributes.includeEndAt,
    days: daysBetween(
      attributes.startAt,
      attributes.endAt,
      attributes.includeStartAt,
      attributes.includeEndAt
    )
  }

  if (trip.budget > 0) return updateBudget(trip, trip)
  if (trip.dailyBudget > 0) return updateDailyBudget(trip, trip)

  return updateUI(trip)
}

const updateUI = (trip) => {
  ui.budget.value = currencyToInput(trip.budget)
  ui.dailyBudget.value = currencyToInput(trip.dailyBudget)
  ui.startAt.valueAsDate = trip.startAt
  ui.includeStartAt.checked = trip.includeStartAt
  ui.endAt.valueAsDate = trip.endAt
  ui.includeEndAt.checked = trip.includeEndAt
  ui.period.innerText = period(trip)

  return trip
}

const period = (trip) => {
  if (trip.budget <= 0) return ''

  const included = '✓'

  return [
    trip.includeStartAt ? included : '',
    dateFormatter(trip.startAt),
    '-',
    trip.includeEndAt ? included : '',
    dateFormatter(trip.endAt)
  ].join(' ')
}

const currencyToInput = (value) => {
  if (value == 0) return ''

  return round(value)
}
