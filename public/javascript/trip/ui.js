export const ui = {
  edit: document.getElementById('trip-edit'),
  form: document.getElementById('trip-form'),
  period: document.getElementById('trip-period'),
  budget: document.getElementById('trip-budget'),
  dailyBudget: document.getElementById('trip-daily-budget'),
  startAt: document.getElementById('trip-start-at'),
  endAt: document.getElementById('trip-end-at'),
  includeStartAt: document.getElementById('trip-start-at-including'),
  includeEndAt: document.getElementById('trip-end-at-including')
}
