import { ui } from './ui.js'
import {
  currencyFormatter,
  dateFormatter,
  timeFormatter,
  datetimeFormatter,
} from '../lib/formatters.js'

export const startNew = () => {
  clearForm()
  ui.description.focus()
}

export const load = (state) => {
  clearForm()
  updateList(state)
}

export const save = (state, transaction) => {
  if (isNaN(transaction.value) || transaction.value <= 0) {
    return state
  }

  const trip = { ...state }

  if (transaction.id == "") {
    transaction.id = crypto.randomUUID()
  }

  trip.transactions[transaction.id] = {
    ...transaction,
    timestamp: datetimeFormatter(new Date(transaction.timestamp))
  }

  clearForm()
  updateList(trip)

  return trip
}

export const edit = (state, { id }) => {
  const transaction = state.transactions[id]

  ui.id.value = transaction.id
  ui.value.value = transaction.value
  ui.description.value = transaction.description
  ui.timestamp.value = datetimeFormatter(transaction.timestamp)

  Array
    .from(ui.categories)
    .find(e => e.value == transaction.category)
    .checked = true

  ui.remove.classList.toggle('hidden', false)

  ui.modal.showModal()
}

export const remove = (state, transaction) => {
  const trip = { ...state }
  delete trip.transactions[transaction.id]

  updateList(trip)

  return trip
}

export const clear = () => {
  ui.transactionList.innerHTML = ''
}

const groupByDate = (state) =>
  Object
    .entries(state.transactions)
    .reduce((result, [_, transaction]) => {
      const key = dateFormatter(transaction.timestamp)

      if (!result[key]) {
        result[key] = { total: transaction.value, list: [transaction] }
      } else {
        result[key].total += transaction.value
        result[key].list.push(transaction)
      }

      result[key]['overspent'] = result[key].total > state.dailyBudget

      return result
    }, {})

const updateList = (state) => {
  const data = groupByDate(state)

  clear()

  Object.keys(data)
    .sort((a, b) => {
      return a <= b
    })
    .forEach((key) => {
      const group = createGroup(data, key)
      const groupList = document.createElement('ul')

      ui.transactionList.appendChild(group)

      group.appendChild(groupList)

      data[key].list
        .sort((a, b) => (new Date(a.timestamp) <= new Date(b.timestamp)))
        .forEach((transaction) => addToGroup(transaction, groupList))
    })
}

const createGroup = (data, key) => {
  const group = document.createElement('div')
  const groupTitle = document.createElement('h3')

  group.appendChild(groupTitle)

  group.classList.add('transaction-group')

  groupTitle.innerHTML = `
    <span>${key}</span>
    <span class="group-total ${data[key].overspent ? 'overspent' : ''}">
      ${currencyFormatter(data[key].total)}
    </span>`

  return group
}

const addToGroup = (transaction, group) => {
  const item = document.createElement('li')

  group.appendChild(item)

  item.outerHTML = `
    <li class="transaction-item" id="transaction-${transaction.id}">
      <div class="transaction-item-data">
        <span class="transaction-item-category">
          ${categoryFor(transaction)}
        </span>

        <span>
          <div class="transaction-item-description">
            ${transaction.description}
          </div>
          <div class="transaction-item-time">
            ${timeFormatter(new Date(transaction.timestamp))}
          </div>
        </span>

        <span class="transaction-item-value">
          ${currencyFormatter(transaction.value)}
        </span>
      </div>
      <a
        href="#"
        class="button transaction-item-menu-trigger"
        data-transaction-id="${transaction.id}">
        ⋮
      </a>
    </li>
  `
}

const clearForm = () => {
  ui.id.value = ''
  ui.value.value = ''
  ui.description.value = ''
  ui.timestamp.value = datetimeFormatter(new Date())
  ui.categories.forEach(e => e.checked = false)

  ui.remove.classList.toggle('hidden', true)
}

const categoryFor = (transaction) => {
  switch (transaction.category) {
    case 'food':
      return '🍽️'
    case 'transport':
      return '🚌'
    case 'other':
      return '📷'
    default:
      return '🤷'
  }
}
