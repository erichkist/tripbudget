import { filterFloat } from '../lib/math.js'
import { ui } from './ui.js'
import { dispatch } from '../reducer.js'

export const startEventListners = () => {
  ui.new.addEventListener('click', dispatchTransactionNew)
  ui.form.addEventListener('submit', dispatchTransactionCreate)
  ui.form.addEventListener('reset', dispatchTransactionRemove)

  ui.transactionList.addEventListener('click', dispatchTransactionEdit)
}

const dispatchTransactionNew = () =>
  dispatch('transaction.new')

const dispatchTransactionCreate = () =>
  dispatch('transaction.save', {
    id: ui.id.value,
    description: ui.description.value,
    value: filterFloat(ui.value.value),
    timestamp: ui.timestamp.value,
    category: selectedCategory()
  })

const dispatchTransactionEdit = (event) => {
  if (!event.target.dataset.transactionId) {
    return
  }

  dispatch('transaction.edit', { id: event.target.dataset.transactionId })
}

const dispatchTransactionRemove = (event) => {
  event.stopPropagation()
  event.preventDefault()

  if (confirm("This cannot be undone! is Are you sure?")) {
    dispatch('transaction.remove', { id: ui.id.value })
  }
}

const selectedCategory = () =>
  Array
    .from(ui.categories)
    .find(e => e.checked).value
