export const filterFloat = function(value) {
  if (/^(\-|\+)?([0-9]+(\.[0-9]+)?|Infinity)$/.test(value)) {
    return Number(value)
  }

  return NaN
}

export const round = (value) => Math.trunc(Number(value) * 100) / 100

export const daysBetween = (date1, date2, includeDate1 = false, includeDate2 = false) => {
  const zeroDate1 = zeroHour(date1)
  const zeroDate2 = zeroHour(date2)

  const diff = zeroDate2 - zeroDate1

  if (diff <= 0) return 0

  const days = Math.floor(diff / (1000 * 3600 * 24));

  if (includeDate1 && includeDate2) {
    return days + 1
  } else if (includeDate1 || includeDate2) {
    return days
  } else {
    return days - 1
  }
}

const zeroHour = (value = new Date()) => {
  return new Date(
    Date.UTC(
      value.getUTCFullYear(),
      value.getUTCMonth(),
      value.getUTCDate()
    )
  )
}
