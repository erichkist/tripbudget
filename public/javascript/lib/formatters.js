export const currencyFormatter = (value) => {
  return value.toLocaleString('en-US', {
    style: 'currency',
    currency: 'USD',
    roundingMode: 'floor',
    minimumFractionDigits: 2
  })
}

export const dateFormatter = (value) => {
  const date = (value instanceof Date) ? value : new Date(value)

  return [
    date.getFullYear(),
    (date.getMonth() + 1).toString().padStart(2, '0'),
    date.getDate().toString().padStart(2, '0')
  ].join('-')
}

export const timeFormatter = (value) => {
  const date = (value instanceof Date) ? value : new Date(value)

  return [
    date.getHours().toString().padStart(2, '0'),
    date.getMinutes().toString().padStart(2, '0'),
  ].join(':')
}

export const datetimeFormatter = (value) => {
  return [
    dateFormatter(value),
    timeFormatter(value)
  ].join('T')
}

export const percentFormatter = (value) => {
  const number = Number(value)

  if (number <= 0) return '0%'
  if (number >= 1) return '100%'

  return `${Math.floor(number * 100)}%`
}
