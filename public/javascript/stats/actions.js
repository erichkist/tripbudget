import { daysBetween } from '../lib/math.js'

import { start as uiStarter } from './ui.js'
import {
  dateFormatter,
  currencyFormatter,
  percentFormatter
} from '../lib/formatters.js'

const ui = uiStarter()

export const load = (state) => {
  update(state)
}

export const update = (state) => {
  const stats = buildStats(state)

  ui.daysTotalLabel.innerHTML = stats.daysTotal
  ui.daysRestingLabel.innerHTML = stats.daysResting
  ui.daysUsedLabel.innerHTML = stats.daysUsed
  updatePercent(ui.daysUsedBar, stats.daysUsed, stats.daysTotal)

  ui.budgetTotalLabel.innerHTML = currencyFormatter(stats.budgetTotal)
  ui.budgetRestingLabel.innerHTML = currencyFormatter(stats.budgetResting)
  ui.budgetUsedLabel.innerHTML = currencyFormatter(stats.budgetUsed)
  updatePercent(ui.budgetUsedBar, stats.budgetUsed, stats.budgetTotal)

  ui.todayTotalLabel.innerHTML = currencyFormatter(stats.todayTotal)
  ui.todayRestingLabel.innerHTML = currencyFormatter(stats.todayResting)
  ui.todayUsedLabel.innerHTML = currencyFormatter(stats.todayUsed)
  updatePercent(ui.todayUsedBar, stats.todayUsed, stats.todayTotal)
}

const buildStats = (state) => {
  const today = new Date()

  const transactions = groupTransactions(state, today)
  const spentBeforeToday = state.budget - sumTransactions(transactions.beforeToday)

  let stats = {
    daysTotal: state.days,
    daysUsed: daysUsed(state, today),
    daysResting: 0,

    budgetTotal: state.budget,
    budgetUsed: budgetSpent(state),
    budgetResting: 0,

    todayTotal: 0,
    todayUsed: 0,
    todayResting: 0,
  }

  stats.daysResting = stats.daysTotal - stats.daysUsed

  stats.budgetResting = stats.budgetTotal - stats.budgetUsed

  stats.todayTotal = spentBeforeToday / (stats.daysResting || 1)
  stats.todayUsed = sumTransactions(transactions.today)
  stats.todayResting = stats.todayTotal - stats.todayUsed

  return stats
}

const daysUsed = (state, today) => {
  const startAt = new Date(state.startAt)
  const endAt = new Date(state.endAt)

  if (startAt > today) return 0
  if (endAt < today) return 0

  return daysBetween(startAt, today, true)
}

const budgetSpent = (state) =>
  sumTransactions(Object.entries(state.transactions))

const sumTransactions = (transactions) =>
  transactions.reduce((result, [_, transaction]) => {
    return result + transaction.value
  }, 0)

const groupTransactions = (state, today) => {
  return Object.entries(state.transactions).reduce((result, [id, transaction]) => {
    if (dateFormatter(new Date(transaction.timestamp)) == dateFormatter(today)) {
      result.today.push([id, transaction])
    } else {
      result.beforeToday.push([id, transaction])
    }

    return result
  }, { today: [], beforeToday: [] })
}

const updatePercent = (node, used, total) => {
  if (used == 0 || total == 0) {
    node.style.width = 0
    node.innerHTML = ''
  } else {
    const percent = used / (total)

    node.style.width = percentFormatter(percent)
    node.innerHTML = node.style.width
  }
}
