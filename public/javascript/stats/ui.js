const container = document.querySelector('.stats')
const template = document.getElementById('graph-template')

export const start = () =>
  ['days', 'budget', 'today'].reduce((result, type) => {
    const graph = template.content.cloneNode(true)

    graph.querySelector('.graph').className += ` graph-${type}`
    graph.querySelector('.graph-main-label > label').innerHTML = type

    container.appendChild(graph)

    result[`${type}`] = container.querySelector(`.graph-${type}`)
    result[`${type}TotalLabel`] = container.querySelector(`.graph-${type} .graph-value-label`)
    result[`${type}RestingLabel`] = container.querySelector(`.graph-${type} .graph-resting-label`)
    result[`${type}UsedLabel`] = container.querySelector(`.graph-${type} .graph-used-label`)
    result[`${type}UsedBar`] = container.querySelector(`.graph-${type} #graph-used-bar`)

    return result
  }, {})
