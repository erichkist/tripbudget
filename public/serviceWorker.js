const PWAVersion = 'tripbudget-v1'
const assets = [
  '/',
  '/tripbudget/index.html',
  '/tripbudget/stylesheets/application.css',
  '/tripbudget/javascript/application.js',
  '/tripbudget/images/airplane160.png',
  '/tripbudget/images/airplane512.png',
  '/tripbudget/images/android-chrome-192x192.png',
  '/tripbudget/images/android-chrome-512x512.png',
  '/tripbudget/images/icons/gear.svg'
]

self.addEventListener('install', (event) =>
  event.waitUntil(
    caches.open(PWAVersion).then((cache) =>
      cache.addAll(assets)
    )
  )
)

self.addEventListener('fetch', (event) => {
  event.respondWith((async () => {
    const cache = await caches.open('dynamic-cache')

    try {
      const response = await fetch(event.request)
      cache.put(event.request, response.clone())

      return response

    } catch (error) {
      return await cache.match(event.request)
    }
  })())
})
